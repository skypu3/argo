﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license.
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine.XR.ARFoundation;
using Cysharp.Threading.Tasks;
using Microsoft.Azure.SpatialAnchors;
using Microsoft.Azure.SpatialAnchors.Unity;
using UnityEngine;

namespace Argo.Cloud.Anchor.App
{
    public class SpatialAnchorScript : DemoScriptBase
    {
        internal enum AppState
        {
            DemoStepCreateSession = 0,
            Placing,
            DemoStepSaveCloudAnchor,
            DemoStepSavingCloudAnchor,
            DemoStepStopSession,
            DemoStepDestroySession,
            DemoStepCreateSessionForQuery,
            DemoStepStopWatcher,
            DemoStepStopSessionForQuery,
            DemoStepQueryComplete
        }

        private readonly Dictionary<AppState, DemoStepParams> stateParams = new Dictionary<AppState, DemoStepParams>
        {
            { AppState.DemoStepCreateSession,new DemoStepParams() { StepMessage = "Next: Create Spatial Anchors Session", StepColor = Color.clear }},
            { AppState.Placing, new DemoStepParams() { StepMessage = "Tap a surface to add the Local Anchor.", StepColor = Color.clear}},
            { AppState.DemoStepSaveCloudAnchor,new DemoStepParams() { StepMessage = "Next: Save Local Anchor to cloud", StepColor = Color.yellow }},
            { AppState.DemoStepSavingCloudAnchor,new DemoStepParams() { StepMessage = "Saving local Anchor to cloud...", StepColor = Color.yellow }},
            { AppState.DemoStepStopSession,new DemoStepParams() { StepMessage = "Next: Stop Spatial Anchors Session", StepColor = Color.green }},
            { AppState.DemoStepCreateSessionForQuery,new DemoStepParams() { StepMessage = "Next: Create Spatial Anchors Session for query", StepColor = Color.clear }},
            { AppState.DemoStepStopWatcher,new DemoStepParams() { StepMessage = "Next: Stop Watcher", StepColor = Color.yellow }},
            { AppState.DemoStepStopSessionForQuery,new DemoStepParams() { StepMessage = "No Models Close to You", StepColor = Color.grey }},
            { AppState.DemoStepQueryComplete,new DemoStepParams() { StepMessage = "Delete Anchors and restart", StepColor = Color.clear }}
        };

        private AppState _currentAppState = AppState.DemoStepCreateSession;

        AppState currentAppState
        {
            get
            {
                return _currentAppState;
            }
            set
            {
                if (_currentAppState != value)
                {
                    Debug.LogFormat("State from {0} to {1}", _currentAppState, value);
                    _currentAppState = value;
                    if (spawnedObjectMat != null)
                    {
                        spawnedObjectMat.color = stateParams[_currentAppState].StepColor;
                    }

                    if (!isErrorActive)
                    {
                        feedbackBox.text = stateParams[_currentAppState].StepMessage;
                    }
                    EnableCorrectUIControls();
                }
            }
        }

        private PlatformLocationProvider locationProvider;
        private List<GameObject> allDiscoveredAnchors = new List<GameObject>();
        private readonly List<GameObject> allSpawnedObjects = new List<GameObject>();

        private void EnableCorrectUIControls()
        {
            int nextButtonIndex = 0;
            int enumerateButtonIndex = 2;

            switch (currentAppState)
            {
                case AppState.DemoStepSavingCloudAnchor:
                #if WINDOWS_UWP || UNITY_WSA
                    // Sample disables "Next step" button on Hololens, so it doesn't overlay with placing the anchor and async operations, 
                    // which are not affected by user input.
                    // This is also part of a workaround for placing anchor interaction, which doesn't receive callback when air tapping for placement
                    // This is not applicable to Android/iOS versions.
                    XRUXPicker.Instance.GetDemoButtons()[nextButtonIndex].gameObject.SetActive(false);
                #endif
                    break;
                case AppState.DemoStepStopSessionForQuery:
                    //XRUXPicker.Instance.GetDemoButtons()[enumerateButtonIndex].gameObject.SetActive(true);
                    break;
                default:
                    XRUXPicker.Instance.GetDemoButtons()[nextButtonIndex].gameObject.SetActive(true);
                    XRUXPicker.Instance.GetDemoButtons()[enumerateButtonIndex].gameObject.SetActive(false);
                    break;
            }
        }

        public SensorStatus GeoLocationStatus
        {
            get
            {
                if (locationProvider == null)
                    return SensorStatus.MissingSensorFingerprintProvider;
                if (!locationProvider.Sensors.GeoLocationEnabled)
                    return SensorStatus.DisabledCapability;
                switch (locationProvider.GeoLocationStatus)
                {
                    case GeoLocationStatusResult.Available:
                        return SensorStatus.Available;
                    case GeoLocationStatusResult.DisabledCapability:
                        return SensorStatus.DisabledCapability;
                    case GeoLocationStatusResult.MissingSensorFingerprintProvider:
                        return SensorStatus.MissingSensorFingerprintProvider;
                    case GeoLocationStatusResult.NoGPSData:
                        return SensorStatus.NoData;
                    default:
                        return SensorStatus.MissingSensorFingerprintProvider;
                }
            }
        }

        public SensorStatus WifiStatus
        {
            get
            {
                if (locationProvider == null)
                    return SensorStatus.MissingSensorFingerprintProvider;
                if (!locationProvider.Sensors.WifiEnabled)
                    return SensorStatus.DisabledCapability;
                switch (locationProvider.WifiStatus)
                {
                    case WifiStatusResult.Available:
                        return SensorStatus.Available;
                    case WifiStatusResult.DisabledCapability:
                        return SensorStatus.DisabledCapability;
                    case WifiStatusResult.MissingSensorFingerprintProvider:
                        return SensorStatus.MissingSensorFingerprintProvider;
                    case WifiStatusResult.NoAccessPointsFound:
                        return SensorStatus.NoData;
                    default:
                        return SensorStatus.MissingSensorFingerprintProvider;
                }
            }
        }

        public SensorStatus BluetoothStatus
        {
            get
            {
                if (locationProvider == null)
                    return SensorStatus.MissingSensorFingerprintProvider;
                if (!locationProvider.Sensors.BluetoothEnabled)
                    return SensorStatus.DisabledCapability;
                switch (locationProvider.BluetoothStatus)
                {
                    case BluetoothStatusResult.Available:
                        return SensorStatus.Available;
                    case BluetoothStatusResult.DisabledCapability:
                        return SensorStatus.DisabledCapability;
                    case BluetoothStatusResult.MissingSensorFingerprintProvider:
                        return SensorStatus.MissingSensorFingerprintProvider;
                    case BluetoothStatusResult.NoBeaconsFound:
                        return SensorStatus.NoData;
                    default:
                        return SensorStatus.MissingSensorFingerprintProvider;
                }
            }
        }

        /// <summary>
        /// Start is called on the frame when a script is enabled just before any
        /// of the Update methods are called the first time.
        /// </summary>
        public override void Start()
        {
            Debug.Log(">>Azure Spatial Anchors Demo Script Start");

            base.Start();

            if (!SanityCheckAccessConfiguration())
            {
                return;
            }
            feedbackBox.text = stateParams[currentAppState].StepMessage;

            Debug.Log("Azure Spatial Anchors Demo script started");

            enableAdvancingOnSelect = false;

            EnableCorrectUIControls();

            if (isScanMode)
            {
                _currentAppState = AppState.DemoStepCreateSessionForQuery;
                AdvanceDemo();
            }
        }

        /// <summary>
        /// Callback function if finding the anchors
        /// </summary>
        /// <param name="args"></param>
        protected override void OnCloudAnchorLocated(AnchorLocatedEventArgs args)
        {
            base.OnCloudAnchorLocated(args);

            if (args.Status == LocateAnchorStatus.Located)
            {
                CloudSpatialAnchor cloudAnchor = args.Anchor;

                UnityDispatcher.InvokeOnAppThread(() =>
                {
                    currentAppState = AppState.DemoStepStopWatcher;
                    Pose anchorPose = Pose.identity;

#if UNITY_ANDROID || UNITY_IOS
                    // anchor pose with your camera
                    anchorPose = cloudAnchor.GetPose();
#endif
                    // HoloLens: The position will be set based on the unityARUserAnchor that was located.
                    GameObject spawnedObject = SpawnNewAnchoredObject(anchorPose.position, anchorPose.rotation, cloudAnchor);
                    allDiscoveredAnchors.Add(spawnedObject);
                });
            }
        }

        public void OnApplicationFocus(bool focusStatus)
        {
#if UNITY_ANDROID
            // We may get additional permissions at runtime. Enable the sensors once app is resumed
            if (focusStatus && locationProvider != null)
            {
                ConfigureSensors();
            }
#endif
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        public override void Update()
        {
            base.Update();

            if (spawnedObjectMat != null)
            {
                float rat = 0.1f;
                float createProgress = 0f;
                if (CloudManager.SessionStatus != null)
                {
                    createProgress = CloudManager.SessionStatus.RecommendedForCreateProgress;
                }
                rat += (Mathf.Min(createProgress, 1) * 0.9f);
                spawnedObjectMat.color = GetStepColor() * rat;
            }
        }

        public override bool IsPlacingObject()
        {
            return currentAppState == AppState.Placing;
        }

        protected override Color GetStepColor()
        {
            return stateParams[currentAppState].StepColor;
        }

        protected override async void ReturnToLauncher()
        {
            currentAppState = AppState.DemoStepStopSession;
            await AdvanceDemoAsync();

            base.ReturnToLauncher();
        }

        /// <summary>
        /// After save anchor, you need call api to store anchor with gps info into ar cloud
        /// </summary>
        /// <returns></returns>
        protected override async Task OnSaveCloudAnchorSuccessfulAsync()
        {
            await base.OnSaveCloudAnchorSuccessfulAsync();

            // Save GPS information and anchor info into ar cloud with your queryKey
            AnchorModel model = new AnchorModel();
            model.anchorId = currentCloudAnchor.Identifier;
            await ServiceManager.Instance.StartGPS();
            model.latitude = Input.location.lastData.latitude;
            model.longitude = Input.location.lastData.longitude;
            // Demo store customized fields into ar cloud
            model.colorIndex = spawnedObject.GetComponent<ChangeColor>().GetColorIndex();
            if (firstAnchorId != "")
            {
                model.parentAnchorId = firstAnchorId;
            }
            string json = JsonUtility.ToJson(model);
            await ApiManager.Instance.CreateAnchor(json, isFirstAnchor);
            // If this is root anchor that supposed to first save...
            if (isFirstAnchor)
            {
                firstAnchorId = currentCloudAnchor.Identifier;
                isFirstAnchor = false;
            }

            Debug.Log("Anchor created, yay!");
            allSpawnedObjects.Add(spawnedObject);

            // Sanity check that the object is still where we expect
            Pose anchorPose = Pose.identity;

#if UNITY_ANDROID || UNITY_IOS
            anchorPose = currentCloudAnchor.GetPose();
#endif
            // HoloLens: The position will be set based on the unityARUserAnchor that was located.
            //SpawnOrMoveCurrentAnchoredObject(anchorPose.position, anchorPose.rotation);

            if (feedbackBox != null)
            {
                feedbackBox.text = "Saved...Make Next Anchor";
            }

            spawnedObject = null;
            currentCloudAnchor = null;
            Debug.Log("Saved object...");
            currentAppState = AppState.Placing;
            CloudManager.StopSession();
        }

        protected override void OnSaveCloudAnchorFailed(Exception exception)
        {
            base.OnSaveCloudAnchorFailed(exception);
        }

        public async override Task AdvanceDemoAsync()
        {
            if (cts == null)
            {
                cts = new CancellationTokenSource();
            }

            switch (currentAppState)
            {
                case AppState.DemoStepCreateSession:
                    if (CloudManager.Session == null)
                    {
                        // Important: wait to AR session status changed into sessionTracking, then can start ar cloud manager
                        await UniTask.WaitUntil(() => ARSession.state == ARSessionState.SessionTracking, cancellationToken: cts.Token);
                        await CloudManager.CreateSessionAsync();
                    }
                    currentCloudAnchor = null;
                    await CloudManager.StartSessionAsync();
                    // Request device sensor to store location information on anchors (GPS, wifi, bluetooth)
                    locationProvider = new PlatformLocationProvider();
                    CloudManager.Session.LocationProvider = locationProvider;
                    SensorPermissionHelper.RequestSensorPermissions();
                    ConfigureSensors();
                    currentAppState = AppState.Placing;
                    break;
                case AppState.Placing:
                    break;
                case AppState.DemoStepSaveCloudAnchor:
                    currentAppState = AppState.DemoStepSavingCloudAnchor;
                    await SaveCurrentObjectAnchorToCloudAsync();
                    break;
                case AppState.DemoStepStopSession:
                    // Stop watcher, and ar cloud manager
                    CloudManager.StopSession();
                    if (currentWatcher != null)
                    {
                        currentWatcher.Stop();
                        currentWatcher = null;
                    }
                    CleanupSpawnedObjects();
                    await CloudManager.ResetSessionAsync();
                    locationProvider = null;
                    break;
                case AppState.DemoStepCreateSessionForQuery:
                    if (CloudManager.Session == null)
                    {
                        await UniTask.WaitUntil(() => ARSession.state == ARSessionState.SessionTracking, cancellationToken: cts.Token);
                        await CloudManager.CreateSessionAsync();
                        currentCloudAnchor = null;
                        await CloudManager.StartSessionAsync();
                    }
                    locationProvider = new PlatformLocationProvider();
                    CloudManager.Session.LocationProvider = locationProvider;
                    // Request device sensor to query anchors
                    SensorPermissionHelper.RequestSensorPermissions();
                    ConfigureSensors();
                    // Find nearby anchors close to you
                    await StartSearchAnchors();

                    if (queryAnchorList.list.Length > 0)
                    {
                        currentAppState = AppState.DemoStepStopWatcher;
                    }
                    else
                    {
                        currentAppState = AppState.DemoStepStopSessionForQuery;
                    }
                    break;
                case AppState.DemoStepStopWatcher:
                    if (currentWatcher != null)
                    {
                        currentWatcher.Stop();
                        currentWatcher = null;
                    }
                    currentAppState = AppState.DemoStepQueryComplete;
                    break;
                case AppState.DemoStepStopSessionForQuery:
                    CloudManager.StopSession();
                    currentWatcher = null;
                    locationProvider = null;
                    currentCloudAnchor = null;
                    currentAppState = AppState.DemoStepStopSession;
                    break;
                case AppState.DemoStepQueryComplete:
                    await DeleteFoundAnchors(allDiscoveredAnchors);
                    CleanupSpawnedObjects();
                    currentAppState = AppState.DemoStepStopSession;
                    CleanupSpawnedObjects();
                    break;
                default:
                    Debug.Log("Shouldn't get here for app state " + currentAppState.ToString());
                    break;
            }
        }

        protected override void CleanupSpawnedObjects()
        {
            base.CleanupSpawnedObjects();

            foreach (GameObject anchor in allDiscoveredAnchors)
            {
                Destroy(anchor);
            }
            allDiscoveredAnchors.Clear();
        }

        private void ConfigureSensors()
        {
            locationProvider.Sensors.GeoLocationEnabled = SensorPermissionHelper.HasGeoLocationPermission();

            locationProvider.Sensors.WifiEnabled = SensorPermissionHelper.HasWifiPermission();

            locationProvider.Sensors.BluetoothEnabled = SensorPermissionHelper.HasBluetoothPermission();
            locationProvider.Sensors.KnownBeaconProximityUuids = CoarseRelocSettings.KnownBluetoothProximityUuids;
        }

        public override async void GoingNextState()
        {
            await StartSessionAfterPlacing();
            currentAppState = AppState.DemoStepSaveCloudAnchor;
        }
    }
}
