﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServiceManager : Singleton<ServiceManager>
{
    public void StopGPS()
    {
        Input.location.Stop();
    }

    public IEnumerator StartGPS()
    {
        if (!Input.location.isEnabledByUser)
        {
            Debug.Log("isEnabledByUser false");
            yield return false;
        }

        Input.location.Start(10.0f, 10.0f);

        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        if (maxWait < 1)
        {
            yield return false;
        }

        if (Input.location.status == LocationServiceStatus.Failed)
        {
            yield return false;
        }
        else
        {
            yield return true;
        }
    }
}
