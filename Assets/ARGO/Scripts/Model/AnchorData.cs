﻿using System;

[Serializable]
public class AnchorData
{
    public string id;
    public bool isRoot;
    public float distance;
    public int colorIndex;
}

[Serializable]
public class AnchorDataList
{
    public AnchorData[] list;
}
