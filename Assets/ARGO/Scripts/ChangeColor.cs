﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    private static readonly Color[] colors = { Color.blue, Color.red, Color.cyan, Color.green, Color.yellow, Color.magenta };
    private int colorIndex = 0;
    // Start is called before the first frame update
    public void SetColor()
    {
        Material mymat = this.transform.GetComponentInChildren<Renderer>().material;
        colorIndex = Random.Range(0, 5);
        mymat.SetColor("_EmissionColor", colors[colorIndex]);
    }

    public void SetColorIndex(int colorIndex)
    {
        Material mymat = this.transform.GetComponentInChildren<Renderer>().material;
        mymat.SetColor("_EmissionColor", colors[colorIndex]);
        this.colorIndex = colorIndex;
    }

    public int GetColorIndex()
    {
        return colorIndex;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
