﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyScriptTest : MonoBehaviour
{
    [SerializeField]
    private GameObject cube;
    // Start is called before the first frame update
    void Start()
    {
        ChangeColor cc = cube.GetComponent<ChangeColor>();
        cc.SetColor();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
