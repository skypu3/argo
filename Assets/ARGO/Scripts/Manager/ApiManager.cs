﻿using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using System.Text;
using Cysharp.Threading.Tasks;

public enum ApiRouteNames
{
    anchor,
}

public class ApiManager : Singleton<ApiManager>
{ 
    const string URL = "https://arcloud.istaging.co/api/";
    // Replace with your query key
    const string QueryKEY = "dUL3UN5SnnAgZt69haGx";

    /// <summary>
    /// Delete anchor data on remote db
    /// </summary>
    /// <param name="anchorId"></param>
    /// <param name="isRoot"></param>
    /// <returns></returns>
    public async Task DeleteAnchor(string anchorId, bool isRoot)
    {
        string url;
        if (isRoot)
        {
            url = URL + ApiRouteNames.anchor.ToString() + "/root/" + anchorId;
        }
        else
        {
            url = URL + ApiRouteNames.anchor.ToString() + "/sub/" + anchorId;
        }
        var request = new UnityWebRequest(url, "DELETE");
        request.SetRequestHeader("query-key", QueryKEY);

        await request.SendWebRequest();
    }
    /// <summary>
    /// Create anchor data on remote db
    /// </summary>
    /// <param name="jsonData"></param>
    /// <param name="isRoot"></param>
    /// <returns></returns>
    public async Task CreateAnchor(string jsonData, bool isRoot)
    {
        string url;
        if (isRoot)
        {
            url = URL + ApiRouteNames.anchor.ToString() + "/root/";
        }
        else
        {
            url = URL + ApiRouteNames.anchor.ToString() + "/sub/";
        }

        using (UnityWebRequest request = new UnityWebRequest(url, "POST"))
        {
            byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonData);
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            request.SetRequestHeader("query-key", QueryKEY);
            request.SetRequestHeader("Content-Type", "application/json");
            request.SetRequestHeader("Accept", "application/json");
            await request.SendWebRequest();
            if (!request.isNetworkError && request.responseCode == 200)
            {
                Debug.Log("Data succesfully send to the server");
            }
            else
            {
                Debug.Log("Error sending data to the server");
            }
        }
    }

    /// <summary>
    /// Get anchors from location distance
    /// </summary>
    /// <param name="radius"></param>
    /// <returns></returns>
    public async UniTask<AnchorDataList> StartNearWatcher(float radius = 0.3f)
    {
        await ServiceManager.Instance.StartGPS();
        string latitude = Input.location.lastData.latitude.ToString();
        string longitude = Input.location.lastData.longitude.ToString();
        ServiceManager.Instance.StopGPS();

        string url = URL + ApiRouteNames.anchor.ToString() + "/near?" + "latitude=" + latitude + "&longitude=" + longitude + "&radius=" + radius;
        var request = UnityWebRequest.Get(url);
        request.SetRequestHeader("query-key", QueryKEY);
        request.timeout = 60;
        await request.SendWebRequest();

        if (!request.isNetworkError && request.responseCode == 200)
        {
            Debug.Log("Data succesfully get from the server");
            var jsonData = request.downloadHandler.text;
            var anchors = JsonUtility.FromJson<AnchorDataList>(jsonData);
            return anchors;
        }
        else
        {
            Debug.Log("Error sending data to the server");
            return null;
        }
    }
}
