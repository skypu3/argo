﻿//-----------------------------------------------------------------------
// <copyright originalFile="AndyPlacementManipulator.cs" company="Google">
// <renamed file="ARPlacementInteractable.cs">
//
// Copyright 2018 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// </copyright>
//-----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using Argo.Cloud.Anchor.App;

namespace UnityEngine.XR.Interaction.Toolkit.AR
{
    /// <summary>
    /// UnityEvent that responds to changes of hover and selection by this interactor.
    /// </summary>
    [Serializable]
    public class MyARObjectPlacedEvent : UnityEvent<MyARPlacementInteractable, GameObject> { }

    /// <summary>
    /// Controls the placement of Andy objects via a tap gesture.
    /// </summary>
    public class MyARPlacementInteractable : ARBaseGestureInteractable
    {
        //[SerializeField]
        //[Tooltip("A GameObject to place when a raycast from a user touch hits a plane.")]
        //GameObject m_PlacementPrefab;
        ///// <summary>
        ///// A GameObject to place when a raycast from a user touch hits a plane.
        ///// </summary>
        //public GameObject placementPrefab { get { return m_PlacementPrefab; } set { m_PlacementPrefab = value; } }


        [SerializeField, Tooltip("Called when the this interactable places a new GameObject in the world.")]
        MyARObjectPlacedEvent m_OnObjectPlaced = new MyARObjectPlacedEvent();
        /// <summary>Gets or sets the event that is called when the this interactable places a new GameObject in the world.</summary>
        public MyARObjectPlacedEvent onObjectPlaced { get { return m_OnObjectPlaced; } set { m_OnObjectPlaced = value; } }

        static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();
        public static GameObject s_TrackablesObject;
        private DemoScriptBase anchor;

        void Start()
        {
            anchor = GameObject.Find("AzureSpatialAnchors").GetComponent<DemoScriptBase>();
            if (anchor == null)
            {
                Debug.LogError("Anchor script is missing");
            }
        }

        /// <summary>
        /// Returns true if the manipulation can be started for the given gesture.
        /// </summary>
        /// <param name="gesture">The current gesture.</param>
        /// <returns>True if the manipulation can be started.</returns>
        protected override bool CanStartManipulationForGesture(TapGesture gesture)
        {
            // Allow for test planes
            if (gesture.TargetObject == null || gesture.TargetObject.layer == 9)
                return true;

            return false;
        }

        /// <summary>
        /// Function called when the manipulation is ended.
        /// </summary>
        /// <param name="gesture">The current gesture.</param>
        protected override void OnEndManipulation(TapGesture gesture)
        {
            if (gesture.WasCancelled)
                return;

            // If gesture is targeting an existing object we are done.
            // Allow for test planes
            if (gesture.TargetObject != null && gesture.TargetObject.layer != 9)
                return;

            // If it's not placement mode or there is placement object, don't need to place!
            if (!anchor.IsPlacingObject() || anchor.getPlacingObject() != null)
                return;

            // Raycast against the location the player touched to search for planes.
            if (GestureTransformationUtility.Raycast(gesture.startPosition, s_Hits, TrackableType.PlaneWithinPolygon))
            {
                var hit = s_Hits[0];

                // Use hit pose and camera pose to check if hittest is from the
                // back of the plane, if it is, no need to create the anchor.
                if (Vector3.Dot(Camera.main.transform.position - hit.pose.position,
                        hit.pose.rotation * Vector3.up) < 0)
                    return;

                var placementObject = GameObject.Instantiate(anchor.AnchoredObjectPrefab, hit.pose.position, hit.pose.rotation);
                placementObject.GetComponent<ChangeColor>().SetColor();
                //// Create anchor to track reference point and set it as the parent of placementObject.
                //// TODO: this should update with a reference point for better tracking.
                var anchorObject = new GameObject("PlacementAnchor");
                anchorObject.transform.position = hit.pose.position;
                anchorObject.transform.rotation = hit.pose.rotation;
                placementObject.transform.parent = anchorObject.transform;

                // Lookat object
                Vector3 targetPosition = new Vector3(Camera.main.transform.position.x, placementObject.transform.position.y, Camera.main.transform.position.z);
                placementObject.transform.LookAt(targetPosition);
                anchor.SetupAnchoredObject(placementObject);
                anchor.GoingNextState();

                // Find trackables object in scene and use that as parent
                if (s_TrackablesObject == null)
                    s_TrackablesObject = GameObject.Find("Trackables");
                if (s_TrackablesObject != null)
                    anchorObject.transform.parent = s_TrackablesObject.transform;

                if (m_OnObjectPlaced != null)
                    m_OnObjectPlaced.Invoke(this, placementObject);
            }
        }
    }
}