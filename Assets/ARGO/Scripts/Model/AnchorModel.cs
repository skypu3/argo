﻿using System;

[Serializable]
public class AnchorModel
{
    public string anchorId;
    public float latitude;
    public float longitude;
    public string parentAnchorId;
    // customized fields
    public int colorIndex;
}

